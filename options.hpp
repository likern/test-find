#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_

#include <string>

struct Options
{
	Options() : dir("."), name("*") {};
	std::string dir;
	std::string name;
};

Options parse_input_arguments(int argc, char **argv);
bool match_pattern(const char *regex, const char *str);

#endif