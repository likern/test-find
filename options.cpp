#include <getopt.h>
#include <stdexcept>
#include "options.hpp"

using namespace std;

Options parse_input_arguments(int argc, char **argv)
{
	if (!argc || !argv)
	{
		throw std::runtime_error("Invalid options");
	}

	Options option;
	static struct option lopts[] = {
		{"name", required_argument, 0, 'n'},
		{0, 0, 0, 0}
	};

	int ch;
	static int count = 0;
	while((ch = getopt_long_only(argc, argv, "", lopts, NULL)) != -1) {
		if (count++ > 1)
		{
			throw std::runtime_error("Too much options");
		}

		switch (ch) {
			case 'n':
				option.name = optarg;
			break;
			default:
				throw std::runtime_error("Invalid options");
		}
	}

	if (optind < argc)
	{
		// has non-option elements
		count = 0;
		while (optind < argc)
		{
			if (count)
			{
				throw std::runtime_error("Possible only one -name option");
			}
			option.dir = argv[optind++];
			++count;		
		}
	}
	return option;
}

bool match_pattern(const char *regex, const char *str)
{
	const char *itreg = regex;
	const char *itstr = str;
	while (1)
	{
		// special case "*" - all strings match
		if (regex[0] == '*' && regex[1] == '\0')
		{
			return true;
		}

		if ((*itstr == '\0' && *itreg != '\0') ||
			(*itstr != '\0' && *itreg == '\0'))
		{
			return false;
		} else if (*itstr == '\0' && *itreg == '\0')
		{
			return true;
		}
		else
		{
			if ((*itstr == *itreg) || 
				(*itreg == '?'))
			{
				++itstr; ++itreg;
				continue;
			}
			return false;
		}
	}
}