# README #

Example find utility.

### How to build ###

* cmake .
* make
and you will get tfind binary.

### Limitations ###

* When encounter with denied permissions - print it and exit.

### Options ###
* -name "pattern" (with '?' mark)