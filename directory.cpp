#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstring>
#include <string>
#include <vector>
#include <queue>
#include <stdexcept>
#include "directory.hpp"
#include "options.hpp"

using namespace std;

Dir::Dir(const std::string &base) : path(base)
{
	errno = 0;
	DIR *res = opendir(path.c_str());
	if (!res)
	{
		std::string err = strerror(errno);
		throw std::runtime_error(err);
	}
	dir = res;
}

Dir& Dir::operator=(const Dir &other)
{
	if (this == &other)
	{
		return *this;
	}

	errno = 0;
	if (closedir(dir))
	{
		throw std::runtime_error(strerror(errno));
	}

	path = other.path;
	DIR *res = opendir(path.c_str());
	if (!res)
	{
		std::string err = strerror(errno);
		throw std::runtime_error(err);
	}
	dir = res;
	return *this;
}

Dir::~Dir()
{
	closedir(dir);
}

std::string Dir::current()
{
	return path;
}

std::vector<std::string> Dir::find_if(const string &pattern)
{
	vector<string> matches;
	queue<string> dirs;

	// find's default -P option - 
	// never follow symbolic links
	string tmp_str;
	if (is_link(path))
	{
		tmp_str = path;
	}
	else
	{
		tmp_str = bname(path);
		dirs.push(path);
	}

	if (match_pattern(pattern.data(),tmp_str.data()))
	{
		matches.push_back(path);
	}
	
	Dir dr(path);
	bool dir_changed = false;

	while(!dirs.empty())
	{
		if (dir_changed)
		{
			dr = dirs.front();
		}

		errno = 0;
		struct dirent *dinfo = readdir(dr.dir);
		int tmp_errno = errno;
		if (dinfo)
		{
			dir_changed = false;
			if (dinfo->d_name == string(".") ||
				dinfo->d_name == string(".."))
			{
				continue;
			}

			// Next element
			// full_path = dr.path + "/" + dinfo->d_name;
			string full_path = join_path(dr.path, dinfo->d_name);
			if (match_pattern(pattern.data(), dinfo->d_name))
			{
				matches.push_back(full_path);
			}

			if (is_dir(full_path, dinfo))
			{
				dirs.push(full_path);
			}
		} else if (!dinfo && !tmp_errno)
		{
			// End of directory stream
			dir_changed = true;
			dirs.pop();
		} else
		{
			// Get real error
			throw std::runtime_error(strerror(tmp_errno));
		}
	}
	return matches;
}

bool Dir::is_dir(const string &path, dirent *dentry)
{
	if (!dentry) {
		throw std::runtime_error("Invalid arguments");
	}

	if (dentry->d_type == DT_DIR) {
		return true;
	} else if (dentry->d_type == DT_UNKNOWN) {
		struct stat buf;
		if (lstat(path.c_str(), &buf)) {
			throw std::runtime_error(strerror(errno));
		}

		if (S_ISDIR(buf.st_mode)) {
			return true;
		}
	}
	return false;
}

bool Dir::is_link(std::string &pth)
{
	struct stat buf;
	if (lstat(pth.c_str(), &buf))
	{
		throw std::runtime_error(strerror(errno));
	}

	if (S_ISLNK(buf.st_mode))
	{
		return true;
	}
	return false;
}

std::string Dir::join_path(const std::string &dr, const std::string &base)
{
	if (dr.back() == '/')
	{
		return dr + base;
	}
	return dr + "/" + base;
}

std::string Dir::bname(std::string &pth)
{
	errno = 0;
	char *abs_path = realpath(pth.c_str(), nullptr);
	if (abs_path)
	{
		string tmp = basename(abs_path);
		free(abs_path);
		return tmp;
	}
	throw std::runtime_error(strerror(errno));
}