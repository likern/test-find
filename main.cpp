#include <iostream>
#include "directory.hpp"
#include "options.hpp"

using namespace std;

int main(int argc, char **argv) 
{
    try 
    {
        Options opts = parse_input_arguments(argc, argv);
        Dir dir(opts.dir);
        auto results = dir.find_if(opts.name);
        for (auto i : results)
        {
            cout << i << endl;
        }
    }
    catch (exception &error)
    {
        cout << error.what() << endl;
        exit(EXIT_FAILURE);
    }
    catch (...)
    {
        cout << "Not recognized error" << endl;
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}