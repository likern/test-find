// tests.cpp
#include "whattotest.cpp"
#include <gtest/gtest.h>

TEST(InputArgs, InvalidArguments1) {
    char* argv[] = {"find"};
    int argc = -1;
    ASSERT_ANY_THROW(parse_input_arguments(argc, argv));
}

TEST(InputArgs, InvalidArguments2) {
    char* argv[] = {"find"};
    int argc = 0;
    ASSERT_ANY_THROW(parse_input_arguments(argc, argv));
}

TEST(InputArgs, InvalidArguments3) {
    char* argv[] = {"find", "-name"};
    int argc = 2;
    ASSERT_ANY_THROW(parse_input_arguments(argc, argv));
}

TEST(InputArgs, CorrectArguments1) {
    char* argv[] = {"find"};
    int argc = 1;
    Options opt = parse_input_arguments(argc, argv);
    EXPECT_EQ(".", opt.dir);
    EXPECT_EQ("*", opt.name);
}

TEST(InputArgs, CorrectArguments2) {
    char* argv[] = {"find", "/tmp/dir"};
    int argc = 2;
    Options opt = parse_input_arguments(argc, argv);
    EXPECT_EQ("/tmp/dir", opt.dir);
    EXPECT_EQ("*", opt.name);
}

TEST(InputArgs, CorrectArguments3) {
    char* argv[] = {"find", "-name", "^Test?#[123]"};
    int argc = 3;
    Options opt = parse_input_arguments(argc, argv);
    EXPECT_EQ(".", opt.dir);
    EXPECT_EQ("^Test?#[123]", opt.name);
}

TEST(InputArgs, CorrectArguments4) {
    char* argv[] = {"find", "-name", "^Test?#[123]", "/tmp/dir"};
    int argc = 4;
    Options opt = parse_input_arguments(argc, argv);
    EXPECT_EQ("/tmp/dir", opt.dir);
    EXPECT_EQ("^Test?#[123]", opt.name);
}

TEST(Options, DefaultOptions) {
    Options opt;
    EXPECT_EQ(".", opt.dir);
    EXPECT_EQ("*", opt.name;
}

TEST(Directory, CorrectCopyAndAssignConstructor1) {
	Dir dir("/tmp");
    ASSERT_NO_THROW({
    	Dir copy_dir = dir;
    	Dir assign_dir;
    	assign_dir = dir;
    });
}

TEST(Directory, CorrectCopyAndAssignConstructor2) {
	Dir dir("/tmp");
	Dir copy_dir = dir;
    Dir assign_dir;
    assign_dir = dir;
    EXPECT_EQ("/tmp", copy_dir.current());
    EXPECT_EQ("/tmp", assign_dir.current());
}

TEST(Directory, CorrectCopyAndAssignConstructor3) {
	Dir dir("/tmp");
	ASSERT_NO_THROW({
    	dir = dir;
    });
}

TEST(Directory, CorrectCopyAndAssignConstructor4) {
	Dir dir("/tmp");
	dir = dir;
	EXPECT_EQ("/tmp", dir.current());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}