cmake_minimum_required (VERSION 2.8)
project (UNIT_FIND CXX)

find_package(GTest REQUIRED)
include_directories ("${GTEST_INCLUDE_DIRS}")
include_directories ("${PROJECT_SOURCE_DIR}")
include_directories ("${PROJECT_SOURCE_DIR}/..")

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -I/usr/include -O0")
message (STATUS "Compiler flags: ${CMAKE_CXX_FLAGS}")
message (STATUS "Source Dir: ${CMAKE_SOURCE_DIR}")

add_executable (unit-find
                test.cpp)
target_link_libraries (unit-find "${GTEST_LIBRARIES}" pthread)