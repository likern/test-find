#ifndef _DIRECTORY_HPP_
#define _DIRECTORY_HPP_

#include <sys/types.h>
#include <dirent.h>
#include <string>
#include <vector>

class Dir final
{
public:
	Dir(const std::string &base);
	Dir(const Dir &dr) : Dir(dr.path) {}; 
	Dir& operator=(const Dir&);
	~Dir();
	std::vector<std::string> find_if(const std::string &pattern);
	std::string current();
private:
	bool is_dir(const std::string &path, dirent *dentry);
	bool is_link(std::string &pth);
	std::string join_path(const std::string &dir, const std::string &name);
	std::string bname(std::string &path);
	std::string path;
	DIR *dir;
};

#endif